package com.ictpro.hib1;

import com.sun.xml.bind.v2.model.core.ID;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "relation_type")
public class RelationType {

    @Id
    @Column(name = "id_relation_type")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // integer NOT NULL,

    @Column(name = "name", nullable = false, length = 200)
    private String name; // character varying(200) NOT NULL

    protected RelationType() {
    }

    public RelationType(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RelationType that = (RelationType) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "RelationType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
