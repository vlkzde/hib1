package com.ictpro.hib1;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "relation")
public class Relation {

    @Id
    @Column(name = "id_relation")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // integer NOT NULL,

    @Column(name = "id_person1", nullable = false)
    private Long person1; // integer NOT NULL,

    @Column(name = "id_person2", nullable = false)
    private Long person2; // integer NOT NULL,

    @Column(name = "description", nullable = false, length = 200)
    private String description; // character varying(200) NOT NULL,

    @ManyToOne()
    @JoinColumn(name = "id_relation_type", nullable = false)
    private RelationType type; // integer NOT NULL

    protected Relation() {
    }

    public Relation(Long person1, Long person2, String description, RelationType relationType) {
        this.person1 = person1;
        this.person2 = person2;
        this.description = description;
        this.type = relationType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPerson1() {
        return person1;
    }

    public void setPerson1(Long person1) {
        this.person1 = person1;
    }

    public Long getPerson2() {
        return person2;
    }

    public void setPerson2(Long person2) {
        this.person2 = person2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RelationType getType() {
        return type;
    }

    public void setType(RelationType relationType) {
        this.type = relationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Relation relation = (Relation) o;
        return Objects.equals(person1, relation.person1) &&
                Objects.equals(person2, relation.person2) &&
                Objects.equals(description, relation.description) &&
                Objects.equals(type, relation.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person1, person2, description, type);
    }

    @Override
    public String toString() {
        return "Relation{" +
                "id=" + id +
                ", person1=" + person1 +
                ", person2=" + person2 +
                ", description='" + description + '\'' +
                ", relationType=" + type +
                '}';
    }
}
