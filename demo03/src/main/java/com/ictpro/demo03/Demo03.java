/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ictpro.demo03;

import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.mapping.MetadataSource;
/**
 *
 * @author Student
 */
public class Demo03 {
    
    public static void main(String ... args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.ictpro.hib1");
        try {
            
            EntityManager em = emf.createEntityManager();
            try {
                List<Location> result = em.createQuery("from Location", Location.class)
                        .getResultList();
                for (Location loc: result) {
                    System.out.println(loc);
                }
            } finally {
                em.close();
            } 
        } finally {
            emf.close();
        }
        /*
        try (Session session = sessionFactory.openSession()) {
            //session.beginTransaction();
            session.createQuery("from Location as loc where loc.city = :cityName")
                    .setParameter("cityName", "Brno")
                    .getResultStream()
                    .forEach(System.out::println);
            //session.getTransaction().commit();
        } */
        
        
    }
}
