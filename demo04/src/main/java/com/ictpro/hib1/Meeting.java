package com.ictpro.hib1;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "meeting")
public class Meeting {

    @Id
    @Column(name = "id_meeting")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // integer NOT NULL,

    @Column(name = "start", nullable = false)
    private LocalDateTime start; // timestamp with time zone NOT NULL,

    @Column(name = "description", nullable = false, length = 200)
    private String description; // character varying(200) NOT NULL,

    @Column(name = "duration")
    private LocalTime duration; // time without time zone,

    @Column(name = "id_location", nullable = false)
    private Long location; // integer NOT NULL

    @ElementCollection
    private Collection<String> tags;
    
    @ManyToMany(mappedBy = "meetings")
    private Collection<Person> persons;
    
    protected Meeting() {
    }

    public Meeting(LocalDateTime start, String description, Long location) {
        this.start = start;
        this.description = description;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalTime getDuration() {
        return duration;
    }

    public void setDuration(LocalTime duration) {
        this.duration = duration;
    }

    public Long getLocation() {
        return location;
    }

    public void setLocation(Long location) {
        this.location = location;
    }

    public Collection<String> getTags() {
        return tags;
    }
   
    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }

    public Collection<Person> getPersons() {
        return persons;
    }

    public void setPersons(Collection<Person> persons) {
        this.persons = persons;
    }
    
    

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meeting meeting = (Meeting) o;
        return Objects.equals(start, meeting.start) &&
                Objects.equals(description, meeting.description) &&
                Objects.equals(duration, meeting.duration) &&
                Objects.equals(location, meeting.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, description, duration, location);
    }

    @Override
    public String toString() {
        return "Meeting{" + "id=" + id + ", start=" + start + ", description=" + description + ", duration=" + duration + ", location=" + location + ", tags=" + tags + ", persons=" + persons.size() + '}';
    }


}
