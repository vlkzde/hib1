/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ictpro.demo02;

import java.util.Arrays;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.mapping.MetadataSource;
/**
 *
 * @author Student
 */
public class Demo02 {
    
    public static void main(String ... args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        SessionFactory sessionFactory = new MetadataSources(registry)
                .buildMetadata()
                .buildSessionFactory();
        
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Location location = new Location();
            location.setCity("Brno");
            location.setName("Domov");
            location.setCountry("CR");
            System.out.println("Save location, id=" + session.save(location));
            session.getTransaction().commit();
        }   
        
        try (Session session = sessionFactory.openSession()) {
            //session.beginTransaction();
            session.createQuery("from Location as loc where loc.city = :cityName")
                    .setParameter("cityName", "Brno")
                    .getResultStream()
                    .forEach(System.out::println);
            //session.getTransaction().commit();
        } 
        
    }
}
